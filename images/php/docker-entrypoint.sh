#!/bin/bash

if [ ! -f "/app/install.lok" ]
then
 touch /app/install.lok
 echo "Realizando os procedimentos de instalação..."
 cd /app
 chmod 777 storage/
 chmod 777 storage/logs/
 php composer.phar install

 /wait-for-it.sh bk_db:3306 -- echo "O banco está no ar. Woho!"
 php artisan migrate
 php artisan db:seed

fi

php-fpm
