#!/bin/bash

if [ ! -f "/app/install.lok" ]
then
 touch /app/install.lok
 echo "Realizando os procedimentos de instalação..."
 ln -s /usr/bin/nodejs /usr/bin/node
 echo '{ "allow_root": true }' > /root/.bowerrc
 cd /app
 npm install
 npm run bower
fi

nginx -g "daemon off;"
