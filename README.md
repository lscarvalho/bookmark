Sistema *BookM4U*
===================

O sistema consiste em uma *API* feita com PHP/Lumen e um *Front* em Angular utilizando *Docker*.

*Mais informações sobres as tecnologias utilizadas em cada sistema, favor verificar nas pastas apps/api e apps/front.*

----------

Para configurar a aplicação, será necessário seguir os passos descritos abaixo:

 - docker-compose up --build -d

O comando acima irá criar os ambientes e instalar as dependências das aplicações (API e Front).

Para utilizar o sistema, basta entrar com a url http://localhost no navegador.

O único usuário disponível após a instalação será o *admin* com a senha *senha*. Esse é o usuário com o perfil *administrador*.
Usuários comuns, deverão registrar no sistema. Após o registro, o mesmo já se manterá logado.

**Observação**: Como o processo de build chama os scripts para a instalação das dependências dos projetos, pode ser que os containers subam e a aplicação ainda não esteja totalmente operacional. Caso isso aconteça, poderá ser observado nos logs *bk_fpm* se o processo de instalação dos pacotes de terceiros já terminou. O arquivo *install.lok* é criado na primeira vez que o docker-entrypoint.sh é chamado para que o mesmo não execute as tarefas de configuração novamente a cada vez que o container for iniciado.
