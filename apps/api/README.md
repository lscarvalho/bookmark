API do sistema *BookM4U* feito no Lumen PHP Framework
===================

Para configurar a aplicação, será necessário seguir os passos descritos abaixo:

1. Verirficar se o servidor web tem acesso de escrita nas pastas.
 - storage/
 - storage/logs/
2. Instalar as dependências do Lumen.
 - php composer.phar install
3. Criar a estrutura da aplicação e popular as tabelas sistêmicas.
- php artisan migrate
- php artisan db:seed

Após a instalação, o mesmo poderá ser acessado na url http://localhost:90

----------

> **Pacotes de terceiros utilizados:**

> - **JWT** Autenticação utilizada no sistema com o [generationtux/jwt-artisan](https://github.com/generationtux/jwt-artisan)
> - **CORS** Habilitado com o  [palanik/lumen-cors](https://github.com/palanik/lumen-cors)

