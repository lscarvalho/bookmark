<?php

use Illuminate\Database\Seeder;
use App\Model\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
                [
                    'login' => 'admin',
                    'password' => app('hash')->make('senha'),
                    'tipo'=> 'A'
                ]);
    }
}
