<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use GenTux\Jwt\GetsJwtToken;

use App\Model\User;
use App\Model\Bookmark;
use App\Policies\BookmarkPolicy;

class AuthServiceProvider extends ServiceProvider
{
    
    use GetsJwtToken;
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        Gate::policy(Bookmark::class, BookmarkPolicy::class);
        
        $this->app['auth']->viaRequest('api', function ($request) {

            // A validação já está sendo feita na rota pelo midleware [jwt]
//            if (!$this->jwtToken()->validate()) {
//                throw new \Exception('Token inválido.');
//            }

            $payload = $this->jwtPayload();
            return User::find($payload['sub']);
        });
    }
}
