<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookmarkController Extends Controller {

    const MODEL = 'App\Model\Bookmark';

    protected $request;
    
    public function get($id) {

        $class = self::MODEL;
        $oBookmark = $class::findOrFail($id);
        $this->authorize('get', $oBookmark);
        
        return $oBookmark;
    }
    
    public function all() {
        
        $class = self::MODEL;
        
        if(app('auth')->user()->isAdministrador()) {
            $collection = $class::with('user')->get();
        } else {
            $collection = $class::where(array('user_id' => app('auth')->user()->id))->get();
        }
        
        return response()->json($collection->groupBy('user_login')->toArray(), Response::HTTP_OK);
    }
    
    public function create(Request $request) {
        
        $class = self::MODEL;
        $this->validate($request, $class::rules());

        $oBookmark = new $class;
        $oBookmark->descricao = $request->descricao;
        $oBookmark->url = $request->url;
        $oBookmark->user_id = app('auth')->user()->id;
        
        $oBookmark->save();

        return response()->json('Registro criado com sucesso.', Response::HTTP_CREATED);
    }
    
    public function update(Request $request, $id) {
        
        $class = self::MODEL;
        $this->validate($request, $class::rules($id));
        
        $oBookmark = $class::find($id);
        $this->authorize('update', $oBookmark);
        
        if (is_null($oBookmark)) {
            return response()->json('Registro não encontrado.', Response::HTTP_NOT_FOUND);
        }
        $oBookmark->update($request->all());
        
        return response()->json('Registro alterado com sucesso.', Response::HTTP_OK);
    }
    
    public function delete($id) {
        
        $class = self::MODEL;
        $oBookmark = $class::find($id);
        
        if (is_null($oBookmark)) {
            return response()->json('Registro não encontrado.', Response::HTTP_NOT_FOUND);
        }
        $this->authorize('delete', $oBookmark);
        $class::destroy($id);

        return response()->json('', Response::HTTP_NO_CONTENT);
    }

}
