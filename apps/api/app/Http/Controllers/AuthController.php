<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use GenTux\Jwt\JwtToken;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {

    public function login(JwtToken $jwt, Request $request) {
        
        $user = User::where(['login' => $request->input('login')])->first();
        if( $user === NULL || (!Hash::check($request->input('password'), $user->password) )) {
            throw new HttpException(401, 'Usuário ou senha inválido.');
        }
        
        $token = $jwt->createToken($user);

        return $token;
    }

}