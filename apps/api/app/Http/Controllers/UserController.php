<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Model\User;
use Illuminate\Support\Facades\Hash;
use GenTux\Jwt\JwtToken;

class UserController Extends Controller {

    const MODEL = 'App\Model\User';
    
    public function all() {
        
        $class = self::MODEL;
        return response()->json($class::all(), Response::HTTP_OK);
    }
    
    public function create(JwtToken $jwt, Request $request) {
        
        $class = self::MODEL;
        $this->validate($request, $class::rules());

        $oModel = new $class;
        $oModel->login = $request->input('login');
        $oModel->password = Hash::make($request->input('password'));
        $oModel->tipo = User::CLIENTE;
        
        $oModel->save();

        // Gerando o token
        $token = $jwt->createToken($oModel);
        
        return response()->json(["msg"=>'Registro criado com sucesso.',"token"=>$token], Response::HTTP_CREATED);
    }
}
