<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

// Autenticação
$app->group(['prefix' => 'auth', 'namespace' => 'App\Http\Controllers'], function () use ($app) {
    $app->post('login', 'AuthController@login');
});

// Registro novo (Cadastro de usuário)
$app->post('usuario', 'UserController@create');

// Rotas protegidas pelo JWT
$app->group(['middleware' => 'jwt', 'namespace' => 'App\Http\Controllers'], function($app) {

    // Usuário
    $app->get('usuario', 'UserController@all');

    // Bookmark
    $app->get('bookmark', 'BookmarkController@all');
    $app->get('bookmark/{id}', 'BookmarkController@get');
    $app->post('bookmark', 'BookmarkController@create');
    $app->put('bookmark/{id}', 'BookmarkController@update');
    $app->delete('bookmark/{id}', 'BookmarkController@delete');
});
