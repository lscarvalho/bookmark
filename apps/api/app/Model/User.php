<?php

namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use GenTux\Jwt\JwtPayloadInterface;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JwtPayloadInterface {

    use Authenticatable,
        Authorizable;

    const ADMINISTRADOR = 'A';
    const CLIENTE = 'C';

    protected $table = "user";
    
    public static function rules($id=null, $xValidation=[]) {
        return array_merge([
            'login' => 'bail|required|min:3|max:150|unique:user,login' . ($id?",$id" : '') . ',',
            'password' => 'required|min:3|max:150'
        ], 
        $xValidation);
    }

    protected $fillable = ['login', 'password', 'tipo'];
    protected $hidden = ['password', 'updated_at', 'remember_token'];
    protected $appends = ['tipo_desc'];

    public function getPayload() {
        return [
            'sub' => $this->id,
            'exp' => time() + 7200,
            'context' => [
                'login' => $this->login,
                'tipo' => $this->tipo
            ]
        ];
    }

    public function isAdministrador() {
        return $this->tipo === User::ADMINISTRADOR;
    }

    public function isCliente() {
        return $this->tipo === User::CLIENTE;
    }

    public function getTipoDescAttribute() {
        return ($this->tipo === User::ADMINISTRADOR) ? 'Administrador' : 'Cliente';
    }

}
