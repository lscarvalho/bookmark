<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bookmark Extends Model {
    
    protected $table = 'bookmark';
    protected $fillable = ['descricao', 'url', 'user_id'];
    protected $dates = ['created_at', 'updated_at'];
    protected $appends = ['user_login', 'user_tipo', 'user_tipo_desc'];
    protected $hidden = ['user'];
    
    public static function rules($id=null, $xValidation=[]) {
        return array_merge([
            'descricao' => 'bail|required|min:3|max:150',
            'url' => 'required|url'
        ], 
        $xValidation);
    }
    
    public function user() {
        return $this->belongsTo('App\Model\User');
    }
    
    public function getUserLoginAttribute() {
        return $this->user->login;
    }
    
    public function getUserTipoAttribute() {
        return $this->user->tipo;
    }
    
    public function getUserTipoDescAttribute() {
        return $this->user->tipo_desc;
    }


}
