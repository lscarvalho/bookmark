<?php

namespace App\Policies;

use App\Model\User;
use App\Model\Bookmark;

class BookmarkPolicy {

    public function get(User $user, Bookmark $bookmark) {
        return ($user->id === $bookmark->user_id);
    }
    
    public function update(User $user, Bookmark $bookmark) {
        return ($user->id === $bookmark->user_id);
    }
    
    public function delete(User $user, Bookmark $bookmark) {
        return ($user->id === $bookmark->user_id);
    }

}
