Sistema *BookM4U* feito em Angular e Bootstrap
===================

----------

Para configurar a aplicação, será necessário seguir os passos descritos abaixo:

 - npm install
 - npm run bower ou bower  install

Após a instalação, o mesmo poderá ser acessado na url http://localhost

**Observação**: Caso deseje rodar a aplicação no celular, o mesmo deverá estar na mesma rede que o host e será necessário alterar a constante  **apiBaseUrl** no arquivo **app/core/constants.js** para usar o **ip** da máquina hospedeira na porta 90.
