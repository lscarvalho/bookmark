(function () {
    'use strict';
    angular.module('app.main')
            .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$location', 'authSrv', 'Flash'];

    function MainCtrl($location, authSrv, Flash) {

        var vm = this;
        
        vm.auth = authSrv;
        vm.flash = Flash;
        vm.isActive = isActive;

        // Chamada a inicialização
        init();

        function init() {
            // Inicializando o AuthService
            authSrv.init();
            // Redireciona para o Auth caso não esteja logado
            if (!authSrv.isLoggedIn()) {
                $location.path('/auth');
            }
        }
        
        function isActive(viewLocation) {
            return viewLocation === $location.path();
        }
        
    }
})();

