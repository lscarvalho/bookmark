(function () {
    'use strict';

    angular.module('app.main', [])
           .config(config);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
                .when('/', {
                    templateUrl: 'app/main/main.html',
                    controller: 'MainCtrl',
                    controllerAs: 'ctrl',
                    authorize: false,
                    tipo: false
                })
                .when('/404', {
                    templateUrl: 'app/main/404.html',
                    controller: 'MainCtrl',
                    controllerAs: 'ctrl',
                    authorize: false,
                    tipo: false
                });
    }

})();