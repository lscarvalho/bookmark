(function () {
    'use strict';

    angular.module('app.registro')
            .controller('registroListCtrl', bookmarkListCtrl);

    bookmarkListCtrl.$inject = ['restSrv', 'Flash'];
    function bookmarkListCtrl(restSrv, Flash) {

        var vm = this;
        vm.isLoaded = true;
        vm.registro = [];

        activate();

        function activate() {
            restSrv.setEntity('usuario');
            getList();
        }

        function getList() {
            vm.isLoaded = false;
            restSrv.getAll()
                    .then(function success(response) {
                        vm.registro = response;
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao recuperar os registros.');
                    })
                    .finally(function () {
                        vm.isLoaded = true;
                    });
        }

    }

})();