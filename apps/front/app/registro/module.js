(function () {
    'use strict';
    angular.module('app.registro', []).config(config);
    
    config.$inject = ['$routeProvider', 'constants'];
    function config($routeProvider, constants) {
        $routeProvider
                .when('/registro', {
                    templateUrl: 'app/registro/list.html',
                    controller: 'registroListCtrl',
                    controllerAs: 'ctrl',
                    authorize: true,
                    tipo: constants.ADMINISTRADOR
                })
                .when('/registro/add', {
                    templateUrl: 'app/registro/form.html',
                    controller: 'registroAddCtrl',
                    controllerAs: 'ctrl',
                    authorize: false,
                    tipo: false
                }).otherwise({
            // Redireciono para a home em caso de rota errada
            redirectTo: '/'
        });
    }
})();
