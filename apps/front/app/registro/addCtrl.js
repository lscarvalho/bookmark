(function () {
    'use strict';

    angular.module('app.registro')
            .controller('registroAddCtrl', registroAddCtrl);

    registroAddCtrl.$inject = ['restSrv', '$location', 'Flash','authSrv'];
    function registroAddCtrl(restSrv, $location, Flash, authSrv) {

        var vm = this;
        vm.cancel = cancel;
        vm.isLoaded = true;
        vm.save = save;
        vm.usuario = {};
        
        activate();
        
        function activate() {
            restSrv.setEntity('usuario');
        }
        
        function cancel() {
            redirect();
        }

        function save() {
            vm.isLoaded = false;
            restSrv.add(vm.usuario)
                    .then(function success(response) {
                        authSrv.setToken(response.data.token);
                        Flash.showSuccess('Registro criado.');
                        $location.path('/bookmark');
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao criar o registro.');
                    })
                    .finally(function (response) {
                        vm.isLoaded = true;
                    });
        }

        function redirect() {
            $location.path('/auth');
        }

    }

})();