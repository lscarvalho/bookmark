(function () {
    'use strict';

    angular.module('app.auth')
            .factory('authSrv', authSrv);

    authSrv.$inject = ['$http', 'constants', '$window', '$location', '$q'];
    function authSrv($http, constants, $window, $location, $q) {

        var auth = {
            user: {
                login: null,
                tipo: constants.CLIENTE
            },
            isLoggedIn: isLoggedIn,
            login: login,
            logout: logout,
            init: activate,
            isAdmin: isAdmin,
            permiteUser: permiteUser,
            setToken: setToken
        };

        function activate() {
            setUserData(parseJwt($window.sessionStorage.token));
        }

        function isLoggedIn() {
            return ($window.sessionStorage.token != undefined && $window.sessionStorage.token != null) || false;
        }

        function login(user) {

            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + '/auth/login', user)
                    .then(success)
                    .catch(fail);

            function success(request) {
                // Criando os dados na Sessão
                setToken(request.data);
                deferred.resolve(true);
            }

            function fail(request) {
                // Logando os dados
                activate();
                deferred.reject(request.data);
            }
            // Retornando a promise
            return deferred.promise;
        }

        function logout() {
            auth.user.login = null;
            auth.user.tipo = constants.CLIENTE;
            delete $window.sessionStorage.token;
            $location.path('/auth');
        }

        function parseJwt(token) {
            if(!token) return {};
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        }

        function setUserData(data) {
            if (data.hasOwnProperty('context')) {
                var user = data.context;
                auth.user.login = user.login;
                auth.user.tipo = user.tipo;
            }
        }
        
        function isAdmin() {
            return auth.user.tipo === constants.ADMINISTRADOR;
        }
        
        function permiteUser(tipo) {
            return auth.user.tipo === tipo;
        }
        
        function setToken(token) {
            delete $window.sessionStorage.token;
            $window.sessionStorage.token = token;
            activate();
        }

        return auth;
    }
})();