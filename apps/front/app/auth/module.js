(function () {
    'use strict';
    
    angular.module('app.auth',[]).config(config);
    
    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        
        $routeProvider
                .when('/auth', {
                    templateUrl: 'app/auth/login.html',
                    controller: 'authLoginCtrl',
                    controllerAs: 'ctrl',
                    authorize: false,
                    tipo: false
                });

    }
})();