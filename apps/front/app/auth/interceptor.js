(function () {

    'use strict';

    angular.module('app.auth')
            .factory('authinterceptor', authinterceptor)
            .config(config);

    authinterceptor.$inject = ['$q', '$location', '$window'];
    function authinterceptor($q, $location, $window) {

        return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($window.sessionStorage.token) {
                            config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                        }
                        return config;
                    },
                    'responseError': function (response) {
                        if (response.status === 401) {
                            delete $window.sessionStorage.token;
                            $location.path('/auth');
                        }
                        if (response.status === 403) {
                            $location.path('/404');
                        }
                        return $q.reject(response);
                    }
                };
    }

    config.$inject = ['$httpProvider'];
    function config ($httpProvider) {
        $httpProvider.interceptors.push('authinterceptor');
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.common = 'Content-Type: application/json';
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    };


})();