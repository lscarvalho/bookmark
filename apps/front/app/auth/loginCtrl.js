(function () {
    'use strict';

    angular.module('app.auth')
            .controller('authLoginCtrl', authLoginCtrl);

    authLoginCtrl.$inject = ['authSrv', '$location', 'Flash'];
    function authLoginCtrl(authSrv, $location, Flash) {
        var vm = this;
        vm.user = {};
        vm.login = login;
        vm.isLoaded = true;

        activate();

        function login() {
            vm.isLoaded = false;
            authSrv.login(vm.user)
                    .then(function success(response){
                        Flash.showSuccess('Logado com sucesso.');
                        redirect();
                    },function error(response){
                        Flash.showError(response);
                    }).finally(function(){
                        vm.isLoaded = true;
                    });
        }
        
        function redirect() {
            $location.path('/bookmark');
        }
        
        function activate() {
            
            if (authSrv.isLoggedIn()) {
                redirect();
            }
        }
    }

})();