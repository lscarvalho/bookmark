(function () {
    'use strict';

    angular.module('app.bookmark')
            .controller('bookmarkAddCtrl', bookmarkAddCtrl);

    bookmarkAddCtrl.$inject = ['restSrv', '$location', 'Flash'];
    function bookmarkAddCtrl(restSrv, $location, Flash) {

        var vm = this;
        vm.bookmark = {};
        vm.btnLblAcao = 'Criar';
        vm.cancel = cancel;
        vm.isLoaded = true;
        vm.save = save;

        activate();
        
        function activate() {
            restSrv.setEntity('bookmark');
        }
        
        function cancel() {
            redirect();
        }

        function save() {
            vm.isLoaded = false;
            restSrv.add(vm.bookmark)
                    .then(function success(response) {
                        Flash.showSuccess('Bookmark criado.');
                        redirect();
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao criar o bookmark.');
                    })
                    .finally(function (response) {
                        vm.isLoaded = true;
                    });
        }

        function redirect() {
            $location.path('/bookmark');
        }

    }

})();