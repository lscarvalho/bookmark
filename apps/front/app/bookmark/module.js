(function () {
    'use strict';
    angular.module('app.bookmark', []).config(config);
    
    config.$inject = ['$routeProvider', 'constants'];
    function config($routeProvider, constants) {
        $routeProvider
                .when('/bookmark', {
                    templateUrl: 'app/bookmark/list.html',
                    controller: 'bookmarkListCtrl',
                    controllerAs: 'ctrl',
                    authorize: true,
                    tipo: false
                })
                .when('/bookmark/add', {
                    templateUrl: 'app/bookmark/form.html',
                    controller: 'bookmarkAddCtrl',
                    controllerAs: 'ctrl',
                    authorize: true,
                    tipo:constants.CLIENTE
                })
                .when('/bookmark/edit/:id', {
                    templateUrl: 'app/bookmark/form.html',
                    controller: 'bookmarkEditCtrl',
                    controllerAs: 'ctrl',
                    authorize: true,
                    tipo:constants.CLIENTE
                }).otherwise({
            // Redireciono para a home em caso de rota errada
            redirectTo: '/'
        });
    }
})();
