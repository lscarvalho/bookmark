(function () {
    'use strict';

    angular.module('app.bookmark')
            .controller('bookmarkListCtrl', bookmarkListCtrl);

    bookmarkListCtrl.$inject = ['restSrv', '$location', 'Flash', 'ngDialog', '$scope'];
    function bookmarkListCtrl(restSrv, $location, Flash, ngDialog, $scope) {

        var vm = this;
        vm.add = add;
        vm.bookmark = [];
        vm.edit = edit;
        vm.isLoaded = true;
        vm.remove = remove;

        activate();

        function activate() {
            restSrv.setEntity('bookmark');
            getList();
        }

        function add() {
            $location.path('/bookmark/add');
        }

        function edit(obj) {
            $location.path('/bookmark/edit/' + obj.id);
        }

        function remove(item) {
            ngDialog.openConfirm({
                template: "app/core/confirma.exclusao.html",
                className: 'ngdialog-theme-default custom-width',
                showClose: false,
                closeByDocument: false,
                closeByEscape: false,
                closeByNavigation: true,
                scope: $scope
            }).then(function (data) {
                delItem(item);
            });
        }

        function delItem(data) {
            vm.isLoaded = false;
            restSrv.remove(data)
                    .then(function success(response) {
                        getList();
                        Flash.showSuccess("Bookmark " + data.descricao + " excluido com sucesso.");
                    })
                    .catch(function error(response) {
                        Flash.showError("Erro ao excluir o bookmark " + data.descricao);
                    })
                    .finally(function () {
                        vm.isLoaded = true;
                    });
        }

        function getList() {
            vm.isLoaded = false;
            restSrv.getAll()
                    .then(function success(response) {
                        vm.bookmark = response;
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao recuperar os bookmarks.');
                    })
                    .finally(function () {
                        vm.isLoaded = true;
                    });
        }

    }

})();