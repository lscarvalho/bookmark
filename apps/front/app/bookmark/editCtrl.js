(function () {
    'use strict';

    angular.module('app.bookmark')
            .controller('bookmarkEditCtrl', bookmarkEditCtrl);

    bookmarkEditCtrl.$inject = ['restSrv', '$location', 'Flash', '$routeParams'];
    function bookmarkEditCtrl(restSrv, $location, Flash, $routeParams) {

        var vm = this;
        vm.bookmark = {};
        vm.btnLblAcao = 'Alterar';
        vm.cancel = cancel;
        vm.isLoaded = true;
        vm.save = save;

        activate();

        function activate() {
            restSrv.setEntity('bookmark');
            vm.isLoaded = false;
            restSrv.getById($routeParams.id)
                    .then(function success(response) {
                        vm.bookmark = response;
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao recuperar o bookmark.');
                        redirect();
                    })
                    .finally(function (response) {
                        vm.isLoaded = true;
                    });
        }

        function cancel() {
            redirect();
        }

        function save() {
            vm.isLoaded = false;
            restSrv.edit(vm.bookmark)
                    .then(function success(response) {
                        Flash.showSuccess('Bookmark alterado com sucesso.');
                        redirect();
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao alterar o bookmark.');
                    })
                    .finally(function (response) {
                        vm.isLoaded = true;
                    });
        }

        function redirect() {
            $location.path('/bookmark');
        }

    }

})();