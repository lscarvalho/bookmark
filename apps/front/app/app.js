(function () {
    'use strict';

    angular.module('app', [
        // Angular modules
        'ngRoute',
        // Custom modules
        'app.core',
        'app.main',
        'app.auth',
        'app.registro',
        'app.bookmark',
        // Vendor modules
        'ngDialog'

    ]).run(run);

    run.$inject = ['$rootScope', '$location', 'authSrv'];
    function run($rootScope, $location, authSrv) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            // Rotas que necessitam de autorização(Estar logado)
            if (next.authorize) {
                if (!authSrv.isLoggedIn() ) {
                    $rootScope.$evalAsync(function () {
                        $location.path('/auth/login');
                    });
                }
            }
            // Valida a rota para o perfil do usuário autenticado
            if (next.tipo) {
                if (!authSrv.permiteUser(next.tipo) ) {
                    $rootScope.$evalAsync(function () {
                        $location.path('/404');
                    });
                }
            }
            // Rota não permitida para usuário logado
            if (!next.authorize) {
                if (authSrv.isLoggedIn() ) {
                    $rootScope.$evalAsync(function () {
                        $location.path('/404');
                    });
                }
            }
        });
    }

})();