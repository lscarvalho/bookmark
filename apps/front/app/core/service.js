(function () {
    'use strict';

    angular.module('app.core')
            .factory('restSrv', restSrv);

    restSrv.$inject = ['$http', '$q', 'constants'];
    function restSrv($http, $q, constants) {

        var rest = {
            add: add,
            edit: edit,
            entity: null,
            getAll: getAll,
            getById: getById,
            remove: remove,
            setEntity: setEntity
        };
        
        return rest;

        function setEntity(entity) {
            rest.entity = entity;
        }
        
        function add(data) {
            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + "/" + rest.entity, data)
                    .then(function success(response) {
                        deferred.resolve(response);
                    })
                    .catch(function error(response) {
                        deferred.reject(response);
                    });
            return deferred.promise;
        }

        function edit(data) {
            var deferred = $q.defer();
            $http.put(constants.apiBaseUrl + "/"  + rest.entity + "/" + data.id, data)
                    .then(function success(response) {
                        deferred.resolve(response.data);
                    })
                    .catch(function error(response) {
                        deferred.reject(response.data);
                    });
            return deferred.promise;
        }

        function remove(data) {
            var deferred = $q.defer();
            $http.delete(constants.apiBaseUrl + "/" + rest.entity + "/" + data.id)
                    .then(function success(response) {
                        deferred.resolve(response.data);
                    })
                    .catch(function error(response) {
                        deferred.reject(response.data);
                    });
            return deferred.promise;
        }

        function getAll() {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/" + rest.entity)
                    .then(function success(response) {
                        deferred.resolve(response.data);
                    })
                    .catch(function error(response) {
                        deferred.reject(response.data);
                    });
            return deferred.promise;
        }
        
        function getById(id) {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/" + rest.entity + "/" + id)
                    .then(function success(response) {
                        deferred.resolve(response.data);
                    })
                    .catch(function error(response) {
                        deferred.reject(response.data);
                    });
            return deferred.promise;
        }

    }
})();