(function () {
    
'use strict';

angular.module('app.core')
    .service('Flash', ['$timeout', function($timeout) {
        var flash = this;
        
        
        flash.hideScheduled = false;
        flash.message = null;
        
        flash.show = function(vMessage, vHeader, vClass, vIcon, vTimeout){
            flash.message = {
                    header: vHeader,
                    message: vMessage,
                    class: vClass,
                    icon: vIcon,
                    timeout: vTimeout || 2000
                };
        };
        
        flash.showSuccess = function(message, timeout){
            flash.show(message, "Sucesso!", "alert-success", "icon fa fa-check", timeout);
        };
        
        flash.showAlert = function(message, timeout){
            flash.show(message, "Alerta!", "alert-info", "icon fa fa-info", timeout);
        };
        
        flash.showWarning = function(message, timeout){
            flash.show(message, "Alerta!", "alert-warning", "icon fa fa-warning", timeout);
        };
        
        flash.showError = function(message, timeout){
            flash.show(message, "Erro!", "alert-danger", "icon fa fa-ban", timeout);
        };
        
        flash.hide = function hide() {
            flash.message = null;
            flash.hideScheduled = false;
        };
        
        flash.get = function get() {
            if (flash.message == null) {
                return null;
            }
            if (!flash.hideScheduled) {
                flash.hideScheduled = true;
                $timeout(flash.hide, flash.message.timeout);
            }
            return flash.message.message;
        };
        
        flash.getHeader = function () {
            if (flash.message == null) {
                return null;
            }
            if (!flash.hideScheduled) {
                flash.hideScheduled = true;
                $timeout(flash.hide, flash.message.timeout);
            }
            return flash.message.header;
        };
        
        flash.getClass = function () {
            if (flash.message == null) {
                return null;
            }
            if (!flash.hideScheduled) {
                flash.hideScheduled = true;
                $timeout(flash.hide, flash.message.timeout);
            }
            return flash.message.class;
        };
        
        flash.getIcon = function () {
            if (flash.message == null) {
                return null;
            }
            if (!flash.hideScheduled) {
                flash.hideScheduled = true;
                $timeout(flash.hide, flash.message.timeout);
            }
            return flash.message.icon;
        };
        
        flash.hasMessage = function hasMessage() {
            return flash.message != null;
        };
        
        return {
            showSuccess: flash.showSuccess,
            showAlert: flash.showAlert,
            showWarning: flash.showWarning,
            showError: flash.showError,
            hide: flash.hide,
            get: flash.get,
            hasMessage: flash.hasMessage,
            getHeader: flash.getHeader,
            getClass: flash.getClass,
            getIcon: flash.getIcon
        };
    }]);
    
})();